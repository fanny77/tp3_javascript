function Personnage(nom, PDV, Degats)
{
    this.nom = nom;
    this.PDV = PDV;
    this.Degats = Degats;
	this.attack = function (Personnage) {
		Personnage.PDV -= this.Degats;
		console.log(`${this.nom} attaque ${Personnage.nom} et lui inflige ${this.Degats} points de dégats`)
	}	
	this.fend = function (Personnage) {
		this.PDV -= Personnage.Degats/2
		console.log(`${Personnage.nom} m'attaque mais je me défend et n'ai que ${Personnage.Degats/2} points de dégats`)
	}
	this.decrire = function () {
		console.log(`je m'appelle ${this.nom} , j'ai ${this.PDV} points de vie et j'inflige ${this.Degats} points de dégats.`);
	}
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

function isDefeated(Personnage) {
	let isDefeated = false;
	if (Personnage.PDV <= 0) {
		console.log(Personnage.nom + " a perdu")
		if (Personnage.nom == player.nom) {
			arret = prompt(`${player.nom} est KO. A présent, veux tu : \r 
			1. Recommencer une nouvelle partie \r 
			2. Quitter le jeu`);
			switch (arret) {
				case "1":
					location.reload()
					break;
				case "2":
					break;
				default:
					console.log("Je n’ai pas compris, peux-tu répéter ?");
					break;
			}
		}
		else {
			console.log("Félicitations, tu as vaincu le monstre. Tu es à la deuxième porte. Résous l'énigme.")
			Hangman();
		}
		return isDefeated = true;
	}
	else {
		return false;
	}
}

function combat(monstre, player ) {
			while (!isDefeated(player) && !isDefeated(monstre)) {
			choix2 = prompt(`Tu as choisi la porte bleue. Il y a un ${monstre.nom} à l’intérieur. Que souhaites-tu faire : \r 
			1. Attaquer \r 
			2. Se défendre`);
			switch (choix2) {
				case "1":
					monstre.decrire()
					player.decrire()
						let action = actions[getRandomInt(0,actions.length)];
						if (!isDefeated(monstre)) {
							if (action == "attack") {
								player.attack(monstre)
								monstre.attack(player)
							}
							else{
								monstre.fend(player)
							}
						}
						monstre.decrire()
						player.decrire()
						break;
				case "2":
					monstre.decrire()
					player.decrire()
						let action2 = actions[getRandomInt(0,actions.length)];
						if (!isDefeated(monstre)) {
							if (action2 == "attack") {
								player.fend(monstre)
							}
							else{
								monstre.fend(player)
							}
						}
						monstre.decrire()
						player.decrire()
					break;
				default:
						break;
				}
					resultNotOk = false;
				}
}

/* Hangman */
function HideWord(word) {
	let wordUnrevealed = []; 
	for (let i = 0; i < word.length; i++) {
		wordUnrevealed.push('_')
	}
	return wordUnrevealed;
}
function VerifInputHasOnlyOneChar(InputLetter) {
	return InputLetter.length == 1;
}
function VerifTableAscii(InputLetter) {
	if (InputLetter.codePointAt(0) >= 97 && InputLetter.codePointAt(0) <= 122) {
		return true;
	}
}
function ReturnPosLetters(word, inputLetter) {
	let positions = [];
	let pos = word.indexOf(inputLetter);

	while(pos != -1) {
		positions.push(pos);
		pos++;
		pos = word.indexOf(inputLetter, pos);
	}
	return positions;
}
function ReplaceLetter(wordUnrevealed,letter, positions) {
	positions.forEach(pos => {
			wordUnrevealed[pos] = letter;
		});
		return wordUnrevealed;
	}
function WordIsRevealed(wordUnrevealed) {
	return wordUnrevealed.indexOf("_") == -1
}
function getRandomMysteryWord(MysteryWord) {
	return MysteryWord[Math.floor(Math.random() * MysteryWord.length)]
}

function Hangman() {
	const MysteryWord = ["anticonstitutionnellement","ripoliner","perlimpinpin","baragouin","concupiscent"];
	let InputLetter = "";
	let attempts = 10 
	let WordToGuess = getRandomMysteryWord(MysteryWord);
	let placeholder = HideWord(WordToGuess);
	let isRevealed = false;
	
	while (attempts > 0 && !isRevealed) {
		InputLetter = prompt("Veuillez choisir une lettre, il vous reste " + attempts + " essais\n" + placeholder.join(""));
		if (!VerifInputHasOnlyOneChar(InputLetter)) {
			continue;
		}
		let isAscii = VerifTableAscii(InputLetter)
		let positions = ReturnPosLetters(WordToGuess, InputLetter);
	
		if (isAscii && positions.length > 0) {
			ReplaceLetter(placeholder,InputLetter,positions);
		}
		else {
			attempts--;
		}
	
		isRevealed = WordIsRevealed(placeholder);
	}
	
	if (isRevealed) {
		arret = prompt(`Félicitations, tu as sauvé la princesse. A présent, veux-tu : \n 1-recommencer une nouvelle partie ? \n 2- Quitter le jeu`);
		switch (arret) {
			case "1":
				location.reload()
				break;
			case "2":
				break;
			default:
				console.log("Je n’ai pas compris, peux-tu répéter ?");
				break;
		}
	}
	else {
		arret = prompt(`"Mince, tu n'as pas réussi à sauver la princesse. A présent, veux-tu : \n 1-recommencer une nouvelle partie ? \n 2- Quitter le jeu`);
		switch (arret) {
			case "1":
				location.reload()
				break;
			case "2":
				break;
			default:
				console.log("Je n’ai pas compris, peux-tu répéter ?");
				break;
		}
	}
}



const dragon = new Personnage("dragon",100,20);
const phoenix = new Personnage("phoenix",70,10);
const chien = new Personnage("chien",50,5);
const player = new Personnage(prompt("Bonjour, quel est ton nom ?"),60,20)
const monstres = [dragon,phoenix,chien];
const actions = ["attack","fend"];
let resultNotOk = true

while (resultNotOk) { 
	choix1 = prompt(`Bonjour ${player.nom} , tu es un chevalier parti à l’aventure pour sauver la princesse du donjon. Tu es devant le donjon et il y a 3 portes. Laquelle choisis-tu ? \r
	1 – porte bleue  \r
	2 – porte rouge \r
	3 – porte verte` );
	switch (choix1) {
		case "1":
			const monstre1 = monstres[getRandomInt(0,monstres.length)];
			const playerXMonstre1 = new combat(monstre1, player);
			break;
		case "2":
			const monstre2 = monstres[getRandomInt(0,monstres.length)];
			const playerXMonstre2 = new combat(monstre2, player);
			break;

		case "3":
			const monstre3 = monstres[getRandomInt(0,monstres.length)];
			const playerXMonstre3 = new combat(monstre3, player);
			break;
		default:
			console.log("Je n’ai pas compris, peux-tu répéter ?");
			break;
	}
}


